import unittest


def increment_string(strng):
    num_lst = []
    for char in strng[::-1]:
        if char.isdigit():
            num_lst.append(int(char))
        else:
            break
    delta = len(strng) - len(num_lst)
    added_one_str = str(sum([elem*10**idx for idx,elem in enumerate(num_lst)]) + 1)
    return  strng[:delta] + '0' * (len(num_lst) - len(added_one_str)) + added_one_str

#    num_str = ''.join([str(tup[1]) for tup in pos_num_tup])

class testIncStr(unittest.TestCase):
    def test_init(self):
        self.assertEqual(increment_string("foo"), "foo1")
        self.assertEqual(increment_string("foobar001"), "foobar002")
        self.assertEqual(increment_string("foobar1"), "foobar2")
        self.assertEqual(increment_string("foobar00"), "foobar01")
        self.assertEqual(increment_string("foobar99"), "foobar100")
        self.assertEqual(increment_string("foobar099"), "foobar100")
        self.assertEqual(increment_string(""), "1")

if __name__ == '__main__':
    unittest.main()
